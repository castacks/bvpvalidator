# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/jay/simulation_ws/src/bvpvalidator/include".split(';') if "/home/jay/simulation_ws/src/bvpvalidator/include" != "" else []
PROJECT_CATKIN_DEPENDS = "planning_common;octomap_msgs;octomap_ros;roscpp".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lbvpvalidator".split(';') if "-lbvpvalidator" != "" else []
PROJECT_NAME = "bvpvalidator"
PROJECT_SPACE_DIR = "/home/jay/simulation_ws/src/bvpvalidator/cmake-build-debug/devel"
PROJECT_VERSION = "0.0.0"
