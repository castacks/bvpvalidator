# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/usr/local/include".split(';') if "/usr/local/include" != "" else []
PROJECT_CATKIN_DEPENDS = "planning_common;octomap_msgs;octomap_ros;roscpp".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lbvpvalidator".split(';') if "-lbvpvalidator" != "" else []
PROJECT_NAME = "bvpvalidator"
PROJECT_SPACE_DIR = "/usr/local"
PROJECT_VERSION = "0.0.0"
